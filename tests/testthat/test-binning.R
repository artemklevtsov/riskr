context("Binning")

data(loan_data, package = "riskr")
res1 <- binning(loan_data$age, loan_data$loan_status)
res2 <- binning(loan_data$grade, loan_data$loan_status)

test_that("Object class", {
    expect_is(res1, "binning")
    expect_is(res1$bins, "integer")
    expect_is(res1$labels, "character")
    expect_is(res1$woe, "numeric")
    expect_is(res1$breaks, "numeric")
    expect_is(res1$type, "character")
    expect_is(res2$levels, "list")
})

test_that("Number of bins", {
    expect_equal(res1$bins, 3L)
    expect_equal(res2$bins, 4L)
})

test_that("Type value", {
    expect_equal(res1$type, "numeric")
    expect_equal(res2$type, "factor")
})
