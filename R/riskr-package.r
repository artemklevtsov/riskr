#' @title Functions to Facilitate the Evaluation, Monitoring and Modeling process
#'
#' @description
#' The riskr package facilitate credit scoring taskssuch as measure
#' the scores/models performance, the scoring modelling process, visualize
#' relationships between variables. Also there are functions to compute
#' usual values in the credit scoring like WOE, IV.
#'
#' @name riskr
#' @docType package
NULL
