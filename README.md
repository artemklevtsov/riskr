
<!-- README.md is generated from README.Rmd. Please edit that file -->

[![Build
status](https://gitlab.com/artemklevtsov/riskr/badges/master/pipeline.svg)](https://gitlab.com/artemklevtsov/riskr/commits/master)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

# riskr

The R package which contains a risk management utils.

## Installation

To install the development version the following command can be used:

``` r
install.packages("riskr", repos = "https://artemklevtsov.gitlab.io/riskr/drat")
```

## Usage

First we need to load the package:

``` r
library(riskr)
library(data.table)
```

### Binning

``` r
data(loan_data, package = "riskr")
predictors <- setdiff(names(loan_data), "loan_status")
target <- loan_data$loan_status
bins <- lapply(loan_data[predictors], binning, target = target)
bins <- bins[sapply(bins, "[[", "bins") > 0]
binned_cat <- Map(predict, bins, loan_data[names(bins)])
setDT(binned_cat)
binned_woe <- Map(predict, bins, loan_data[names(bins)], type = "woe")
setDT(binned_woe)
```

### WoE

``` r
woe_data <- woe_table(binned_cat, target == 1L)
woe_data[, .(variable, group, total, bad_percent, woe, iv)]
#>           variable         group total bad_percent      woe         iv
#>  1:       int_rate   [-Inf,6.39]  1871     0.00217  1.27550 0.06425815
#>  2:       int_rate   (6.39,8.38]  5339     0.01083  0.68807 0.06648519
#>  3:       int_rate   (8.38,11.1]  6200     0.02031  0.16897 0.00569687
#>  4:       int_rate   (11.1,13.7]  7208     0.03224 -0.18156 0.00876188
#>  5:       int_rate     (13.7,15]  2634     0.01433 -0.41051 0.01786059
#>  6:       int_rate     (15,16.1]  1573     0.00990 -0.58578 0.02317711
#>  7:       int_rate    (16.1,Inf]  1491     0.01107 -0.79199 0.04323200
#>  8:       int_rate       Missing  2776     0.01007  0.05571 0.00028982
#>  9:          grade       D|E|F|G  4366     0.02863 -0.63647 0.07735682
#> 10:          grade             C  5748     0.02901 -0.32168 0.02314302
#> 11:          grade             B  9329     0.03386  0.05532 0.00096039
#> 12:          grade             A  9649     0.01942  0.69611 0.12260033
#> 13: home_ownership    OTHER|RENT 14789     0.06167 -0.10122 0.00541713
#> 14: home_ownership  MORTGAGE|OWN 14303     0.04926  0.11379 0.00608986
#> 15:     annual_inc  [-Inf,27900]  2712     0.01561 -0.47720 0.02547701
#> 16:     annual_inc (27900,44000]  6526     0.03018 -0.21993 0.01181336
#> 17:     annual_inc (44000,66100]  8941     0.03396  0.00428 0.00000563
#> 18:     annual_inc   (66100,Inf] 10913     0.03118  0.31946 0.03379529
#> 19:            age     [-Inf,22]  4363     0.01949 -0.17999 0.00520956
#> 20:            age      (22,Inf] 24729     0.09143  0.03451 0.00099882
```

### Information value and Cramer’s V

IV and Cramer’s V:

``` r
iv_data <- iv(binned_cat, target)
cramer_data <- cramer_stat(binned_cat, target)
merge(iv_data, cramer_data, by = "variable")[order(-iv)]
#>          variable      iv cramer_v
#> 1:       int_rate 0.22976   0.1451
#> 2:          grade 0.22406   0.1470
#> 3:     annual_inc 0.07109   0.0850
#> 4: home_ownership 0.01151   0.0336
#> 5:            age 0.00621   0.0255
```

### AUC

Predict target with a logistic regression:

``` r
orig_fit <- glm(loan_status ~ ., loan_data, family = binomial())
orig_pred <- predict(orig_fit, loan_data, type = "response")
bin_fit <- glm(target ~ .,  binned_cat, family = binomial())
bin_pred <- predict(bin_fit, binned_cat, type = "response")
woe_fit <- glm(target ~ .,  binned_woe, family = binomial())
woe_pred <- predict(woe_fit, binned_woe, type = "response")
```

Evaluate AUC:

``` r
auc(orig_pred, target == 1L)
#> [1] 0.626
auc(bin_pred, target == 1L)
#> [1] 0.657
auc(woe_pred, target == 1L)
#> [1] 0.656
```

### ROC plot

Plot ROC for the single predictor:

``` r
plot_roc(bin_pred, target == 1L)
#> Registered S3 methods overwritten by 'ggplot2':
#>   method         from 
#>   [.quosures     rlang
#>   c.quosures     rlang
#>   print.quosures rlang
```

![](man/figures/README-roc-plot-1.png)<!-- -->

Plot multiple ROC curves:

``` r
preds <- data.table(orig = orig_pred, binned = bin_pred, woe = woe_pred)
plot_roc(preds, target == 1L)
```

![](man/figures/README-roc-plot2-1.png)<!-- -->

### Precision-recall plot

Plot precision-recall for the single predictor:

``` r
plot_pr(bin_pred, target == 1L)
#> Warning: Removed 1 rows containing missing values (geom_path).
```

![](man/figures/README-unnamed-chunk-2-1.png)<!-- -->

Plot multiple predicitons plots:

``` r
plot_pr(preds, target == 1L)
#> Warning: Removed 1 rows containing missing values (geom_path).
```

![](man/figures/README-unnamed-chunk-3-1.png)<!-- -->

### KS plot

Plot KS for the single predictor:

``` r
plot_ks(bin_pred, target == 1L)
```

![](man/figures/README-unnamed-chunk-4-1.png)<!-- -->

Plot multiple predicitons plots:

``` r
plot_ks(preds, target == 1L)
```

![](man/figures/README-unnamed-chunk-5-1.png)<!-- -->

## Bug reports

Use the following command to go to the page for bug report submissions:

``` r
bug.report(package = "riskr")
```

Before reporting a bug or submitting an issue, please do the following:

  - Make sure that no error was found and corrected previously
    identified. You can use the search by the bug tracker;
  - Check the news list for the current version of the package. An error
    it might have been caused by changes in the package. This can be
    done with `news(package = "riskr", Version ==
    packageVersion("riskr"))` command;
  - Make a minimal reproducible example of the code that consistently
    causes the error;
  - Make sure that the error triggered in the function from the `riskr`
    package, rather than in the code that you pass, that is other
    functions or packages;
  - Try to reproduce the error with the last development version of the
    package from the git repository.

When submitting a bug report please include the output produced by
functions `traceback()` and `sessionInfo()`. This may save a lot of
time.

## License

The `riskr` package is distributed under [Apache
License 2.0](https://www.apache.org/licenses/LICENSE-2.0) license.
